# Python installation

Installing python with pyenv will require special flags, needed by PyInstaller.

Install with:

`env PYTHON_CONFIGURE_OPTS="--enable-shared" pyenv install 3.7.6`


# Development

You need poetry:

Install:
* curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python

Configure:
* poetry config settings.virtualenvs.in-project true
* poetry config settings.virtualenvs.path "."

Create venv:
* cd to reproducibility/bp_repo/bp
* poetry update

Open a poetry shell:
* poetry shell

Execute bp tool:
* python -m bp.bp

# Generating the one binary bp tool

`pyinstaller -y --onefile bp.spec`


# Pipeline Execution on pkh1 server

* For convenience and alignment with the docker executions, where all input files should
  be available under `/input` we have created a symbolic link on pkh1 called `/input`
  that points to our downloaded files.

  * `ln -s /data/disk2/files /input`

  That way, we can test parameter files that mention files under `/input` instead of
  `/data/disk2/files` 