import json
import sys
from pathlib import Path


def get_resource_path():
    # due to the way files are packaged when freezing the app
    # we need a special way to find the location where we're running from
    # https://pyinstaller.readthedocs.io/en/stable/runtime-information.html#using-file-and-sys-meipass
    frozen = getattr(sys, "frozen", False)

    if frozen:
        base_path = getattr(sys, "_MEIPASS")
        # base_path = getattr(sys, "_MEIPASS", path.abspath(path.dirname(__file__)))
        conf_dir = Path(base_path)
    else:
        conf_dir = Path()

    return conf_dir


config_data = Path(get_resource_path(), "bp", "configs", "bp_config.json").read_text()

CONFIG = json.loads(config_data)

STAGE_NAMES = [s for s in CONFIG.get("stages")]
FILESETS = [f for f in CONFIG.get("filesets")]

# Location where all input files will be downloaded at
# A further subdirectory will be created there based on the downloaded fileset
DOWNLOAD_INPUT_FILES_FOLDER = CONFIG.get("config").get("download_input_files_folder")

DEFAULT_INIT_FOLDER_NAME = CONFIG.get("config").get("default_init_folder_name")

URL_SUBS = {
    "cosmo_url": "http://cosmoglobe.uio.no/BeyondPlanck/releases/input",
    "pla_url": "http://pla.esac.esa.int/pla/aio",
}
