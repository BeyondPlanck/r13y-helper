import logging
from pathlib import Path

import toml
from bp.configs.bp_config import get_resource_path

logger = logging.getLogger(__name__)

try:
    c = toml.load(Path(".bp", "config.toml"))
except Exception:
    logger.warning("Could not find local configuration file.")
    logger.warning("Created a default one at `.bp/config.toml`")
    logger.warning("Please, make sure to customize it according to your needs!!! ")
    our_deafult_config = Path(
        get_resource_path(), "bp", "configs", "default_app_config.toml"
    )
    c = toml.load(our_deafult_config)
    dot_bp = Path(".", ".bp")
    dot_bp.mkdir(parents=True, exist_ok=True)
    user_config = Path(dot_bp, "config.toml")
    user_config.write_text(our_deafult_config.read_text())


CAPPED_CONCURRENT_DOWNLOADS = 16
DEFAULT_CONCURRENT_DOWNLOADS = 6

CONCURRENT_DOWNLOADS = c.get("downloads").get("CONCURRENT_DOWNLOADS")

CONCURRENT_DOWNLOADS = (
    DEFAULT_CONCURRENT_DOWNLOADS
    if not CONCURRENT_DOWNLOADS
    else min(CONCURRENT_DOWNLOADS, CAPPED_CONCURRENT_DOWNLOADS)
)

DOWNLOAD_PATH = c.get("downloads").get("download_path")
VERBOSE = c.get("app").get("verbose")


def copy_default_app_config(folder: Path):
    from_file = Path(get_resource_path(), "bp", "configs", "default_app_config.toml")
    to_file = Path(folder, ".bp", "config.toml")
    to_file.parent.mkdir(parents=True)

    to_file.write_text(from_file.read_text())
