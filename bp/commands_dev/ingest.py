import json
import sys
from pathlib import Path
from typing import Dict

import requests
from bp.configs.bp_config import CONFIG, URL_SUBS
from bp.impl.downloader import download_fileset


def ingest_fileset(args: Dict[str, str]):
    """ You can pass a txt file with URLs to have this method check them all.

    If the content of the txt file is of the format:

    some.url.a some.url.b
    ....

    then this method will:

    * Download the given url
    * calculate an md5sum for the file
    * read the actual filesize in bytes
    * write these tree values in the `bp_config.json` file under the given filseset_name

    If the content of the txt file is of the format:

    somehashA some.url.a someIntA
    someHashB some.url.B someIntB
    ....

    then this method will:

    * verify that the url is valid (without downloading the file)
    * check that indeed the filesizes match (from a head request to the fileserver)
    * use the given hash and filesize
    * write these info in the `bp_config.json` file under the given filseset_name

    resulting in a MUCH faster ingestion time in large filesets, since no downloads take
    place.

    The second file format can be easily generated on the hosting machine that contains
    all these urls by using the helper utility bash file at `etc/gen_ingestion_file.sh`
    Just run it in the directory that contains these files:

    $ gen_ingestion_file.sh some/folder > files.txt
    """

    fileset_name = args["fileset_name"]
    urls_file = args["urls_file"]
    skip_urls_check = args["skip_urls_check"]

    if skip_urls_check:
        print("Skipping check for validity of URLs and filesizes.")
        print("Manually check that all provided URLs resolve ok!!")

    urls = Path(urls_file)
    if not urls.exists():
        print(f"Cannot find the {urls_file} file")
        sys.exit(-1)

    c = CONFIG

    # if fileset does not yet exist in filesets
    if fileset_name not in c.get("filesets"):
        c.get("filesets")[fileset_name] = {}

    fileset = c.get("filesets").get(fileset_name)

    files = []
    filled_files = []

    tot_size = 0

    # open given url file
    with open(urls, "r") as input_urls:

        file_no = 0

        # iterate over each line
        for url_line in input_urls:
            file_no += 1

            # skip comment lines
            if url_line[0] != "#":

                url_parts = url_line.split()
                # url file contains only urls
                if len(url_parts) == 1:
                    files.append([url_line.strip(), "", ""])

                # url file contains "md5sum url filesize" parts
                elif len(url_parts) == 3:

                    # since we have all we need (filehash and size)
                    # no need to actually download the file
                    # just make sure the url is correct and the server
                    # returns the proper file size
                    filehash = url_parts[0].strip()
                    url = url_parts[1].strip()
                    filesize = int(url_parts[2].strip())

                    tot_size += filesize

                    filename = url.split("/")[-1]

                    try:

                        if not skip_urls_check:
                            print(f"\rChecking file: {file_no}", end="")
                            reply = requests.head(url, allow_redirects=True)

                            # headers reply should be:
                            # reply_headers = {
                            #     "Content-Type": "some/file",
                            #     "Content-Length": "6347",
                            #     "ETag": '"18cb-4f7c2f94011da"',
                            #     "Accept-Ranges": "bytes",
                            #     "Date": "Mon, 09 Jan 2017 11:23:53 GMT",
                            #     "Last-Modified": "Thu, 24 Apr 2014 05:18:04 GMT",
                            #     "Server": "Apache",
                            #     "Keep-Alive": "timeout=2, max=100",
                            #     "Connection": "Keep-Alive",
                            # }

                            # If url does not resolve correctly
                            if (
                                reply.status_code != 200
                                or int(reply.headers["Content-Length"]) != filesize
                            ):
                                print(type(filesize))
                                print(reply.headers["Content-Length"])
                                print(
                                    f"URL File size of: {reply.headers['Content-Length']} "
                                    f"differs from given: {filesize} for url: {url}"
                                )
                                sys.exit(-1)

                        # take the opportunity to potentially
                        # shorten the given url
                        for shorthand in URL_SUBS:
                            url = url.replace(URL_SUBS[shorthand], shorthand)

                        filled_files.append([url, filename, filehash, filesize])
                    except Exception:
                        print(f"URL {url} failed to resolve")
                        sys.exit(-1)

                # url file contains unknown number of parts
                else:
                    print(f"Cannot parse line: {url_line}")
                    sys.exit(-1)

    # if having only url and having to download them all
    if files:
        fileset["files"] = files
        results = download_fileset(fileset, dryrun=False)

        for r in results:
            filled_files.append(r.get())

    # else, if we already have all the info
    else:
        fileset["total_size"] = tot_size

    fileset["files"] = filled_files

    # Print empty line to scroll over the progress bars.
    print()

    bp_conf_file_loc = Path("bp", "configs", "bp_config.json")
    with open(bp_conf_file_loc, "w") as newConfig:
        newConfig.write(json.dumps(c, indent=4))

