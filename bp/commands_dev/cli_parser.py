"""
Command Line Parsing Module for bp
"""

__all__ = ()

import argparse
import sys

from bp import __version__


def add_ingest_params(subparsers):

    ingest_parser = subparsers.add_parser(
        "ingest", help="Ingest a fileset and populate configuration file"
    )

    # ingest parser
    ingest_parser.add_argument(
        "-f", "--fileset_name", help="Fileset name to ingest", type=str, required=True
    )
    ingest_parser.add_argument(
        "-u",
        "--urls_file",
        help="Name of file containing the fileset urls",
        type=str,
        required=True,
    )

    # USE WITH CAUTION:This param is in place so that we can quickly run
    # the ingestation command without having fully resolved Cosmoglobe
    # (for example) urls. (Maybe Stein has not rsynced yet and we need
    # to keep going.).  Mind the CAUTIION.  The given URLS might by bad.
    ingest_parser.add_argument(
        "-s",
        "--skip-urls-check",
        action="store_true",
        help="Skip check for validity of URLS. Use with CAUTION.",
    )


def add_verify_params(subparsers):

    verify_parser = subparsers.add_parser("verify", help="Verify BP configuration file")


def parse_args(args):
    """
    This function parses the arguments which have been passed from the command
    line, these can be easily retrieved for example by using "sys.argv[1:]".
    It returns a parser object as with argparse.

    Arguments:
    args -- the list of arguments passed from the command line as the sys.argv
            format

    Returns:
    A parser with the provided arguments, which can be used in a
    simpler format
    """
    parser = argparse.ArgumentParser(
        prog="bp_dev", description="BeyondPlanck Reproducibility development tool"
    )

    parser.add_argument(
        "--version", action="version", version="%(prog)s {}".format(__version__)
    )

    subparsers = parser.add_subparsers(help="commands", dest="command")
    subparsers.required = True

    # Main Commands
    add_ingest_params(subparsers)
    add_verify_params(subparsers)

    # making the argument name NOT include a prefix of '-' makes this a
    # positional argument that is a required param
    # run_parser.add_argument("stage", help="Pipeline stage to run", type=str)

    if len(args) == 0:
        parser.print_help(sys.stderr)
        sys.exit(1)

    return parser.parse_args(args)
