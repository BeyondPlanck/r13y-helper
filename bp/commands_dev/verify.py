import json
from pathlib import Path
from typing import Dict

import humanize
from bp.configs.bp_config import CONFIG


def update_filesets_total_size():

    c = CONFIG
    filesets = c.get("filesets")

    print("Updating total file size for each fileset:")

    for aFilesetName in filesets:
        fileset = filesets.get(aFilesetName)
        filesetSize = 0
        for f in fileset.get("files"):
            filesetSize += f[3]  # the fourth item in the file array is the size

        fileset["total_size"] = filesetSize

        prettySize = humanize.naturalsize(filesetSize, gnu=True)
        print(f"    {fileset.get('name')}: {prettySize}")

    bp_conf_file_loc = Path("bp", "configs", "bp_config.json")
    with open(bp_conf_file_loc, "w") as newConfig:
        newConfig.write(json.dumps(c, indent=4))


def check_all_fieldnames_exist():

    c = CONFIG
    filesets = c.get("filesets")

    print("Checking for presence of all required field names:")

    for aFilesetName in filesets:
        print(f"    {aFilesetName}: ", end="")

        fileset = filesets.get(aFilesetName)

        fields = ["name", "location", "files", "total_size"]
        errors = 0
        for aField in fields:
            someVal = fileset.get(aField)
            if not someVal:
                errors += 1
                print(
                    f"\n        Ooops! {aFilesetName} does not contain {aField}", end=""
                )
        if not errors:
            print("OK")
        else:
            print("\n")


def verify_config(args: Dict[str, str]):

    check_all_fieldnames_exist()
    update_filesets_total_size()
