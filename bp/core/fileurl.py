import hashlib
import logging
import re
from pathlib import Path
from typing import List
from bp.configs.bp_config import URL_SUBS

import requests
from tqdm import tqdm

log = logging.getLogger(__name__)


filename_regex = re.compile(
    r'filename\*?=[\'"]?(?:UTF-\d[\'"]*)?([^;\r\n"\']*)[\'"]?;?'
)


class FileUrl(object):
    def __init__(
        self,
        url: str,
        location: Path,
        filename: str = None,
        checksum: str = None,
        filesize: int = None,
    ):

        # Do some keyword substitution to shorten the size of the conf files
        # since we have thousands of repeating files hosted on the same servers
        for shorthand in URL_SUBS:
            url = url.replace(shorthand, URL_SUBS[shorthand])

        self.url = url
        self.location = location
        self.filename = filename
        self.checksum = checksum

        # counter to only try once to get the file headers from the URL
        self.tried_headers = False

        self.headers = None

        self.filesize: int = filesize

    def _get_headers(self):
        """ Internal method. Make a HEAD request and get the response headers.

        From these headers try to derive:
          * filesize
          * filename
        """

        if not self.tried_headers:
            self.tried_headers = True

            # Trying to get some information about the file to download.
            # Without setting Accept-Encoding to None, requests defaults to
            # `gzip, deflate` which generates a header response of
            # `Transfer-Encoding: chunked` that is lacking `Content-Lenght`.
            resp = requests.head(
                self.url, headers={"Accept-Encoding": None}, allow_redirects=True
            )
            self.headers = resp.headers

            log.debug(f"{self.url} headers: {self.headers}")
            size = resp.headers.get("Content-length")
            if size:
                self.filesize = int(size)

            # if we were not initialized with a custom filename
            if not self.filename:
                content_disposition = resp.headers.get("Content-Disposition", None)

                if content_disposition:
                    matched = filename_regex.search(content_disposition)
                    # this is the proposed filename, from the URL, (if any)
                    self.filename = matched.groups()[0]
                else:
                    # In case the filename is not present in the URL headers,
                    # try extracting it from  the url itself.
                    # This works for the files donwloaded from owl.
                    # TODO: Potentially improve the filename is derived from url
                    self.filename = self.url.rsplit("/", 1)[-1]

    def deduce_filename(self) -> str:
        """ Try to deduce the filename of this File"""

        if not self.filename:
            self._get_headers()

        return self.filename

    def deduce_filesize(self) -> int:
        """ Try to deduce the filesize of this File"""

        if not self.filesize:
            self._get_headers()

        return self.filesize

    def getFileName(self, length: int) -> str:
        flen = len(self.filename)
        if flen >= length:
            diff = flen - length
            if diff == 0:
                return self.filename
            else:
                left = int(length / 2 - 1)
                s = f"{self.filename[0:left]}..."
                right = length - len(s)
                return f"{s}{self.filename[-right:]}"
        elif len(self.filename) < length:
            return self.filename + " " * (length - flen)

    def download(self, progress: bool = False, progress_text: str = None):
        log.debug("Start downloading")
        self._get_headers()
        final_file = Path(self.location, self.filename)
        log.debug(f"Final Location: {final_file}")
        if progress and self.filesize:
            chunk_size = 1024 * 1024
            stream = requests.get(self.url, stream=True)

            fname = self.getFileName(length=40)
            with tqdm(
                total=self.filesize,
                initial=0,
                unit="B",
                unit_scale=True,
                desc=f"{progress_text}: {fname}",
            ) as pbar:
                with (open(final_file, "wb")) as f:
                    for chunk in stream.iter_content(chunk_size=chunk_size):
                        f.write(chunk)
                        pbar.update(len(chunk))
        else:
            with (open(final_file, "wb")) as f:
                for chunk in stream.iter_content(chunk_size=chunk_size):
                    f.write(chunk)

        self.checksum = self.calc_checksum()

    def calc_checksum(self):
        """ Calculate the checksum of this File"""

        final_file = Path(self.location, self.filename)
        log.debug(f"Checking checksum of {final_file}")
        hash_algo = hashlib.md5()
        with open(final_file, "rb") as f:
            for chunk in iter(lambda: f.read(1024 * 512), b""):
                hash_algo.update(chunk)
        return hash_algo.hexdigest()

    def exists(self):
        """ Checks if this File exists and if the checksum is correct"""

        self.deduce_filename()
        final_file = Path(self.location, self.filename)
        log.debug(f"Checking if {final_file} exists")

        if final_file.exists():
            if self.checksum:
                valid = self.checksum == self.calc_checksum()
                log.debug(f"{final_file} checksum is {valid}")
                return valid
            else:
                log.debug(f"{final_file} does not have checksum. Assuming missing.")
                return False

        else:
            log.debug(f"{final_file} does not exist.")
            return False

    @classmethod
    def download_with_progress(cls, fields: List[str], location, running_counter):
        aUrl = cls(
            url=fields[0], location=location, filename=fields[1], checksum=fields[2]
        )
        log.debug(f"Asked to download {aUrl.url}")
        if aUrl.exists():
            # print(f"File {aUrl.url} already downloaded.")
            pass
        else:
            aUrl.download(progress=True, progress_text=running_counter)

        return [aUrl.url, aUrl.filename, aUrl.checksum, aUrl.filesize]

    @classmethod
    def check_exists(cls, fields: List[str], location, running_counter):
        aUrl = cls(
            url=fields[0],
            location=location,
            filename=fields[1],
            checksum=fields[2],
            filesize=fields[3],
        )
        log.debug(f"Asked to check if {aUrl.url} exists locally")

        return 0 if aUrl.exists() else aUrl.filesize


if __name__ == "__main__":

    s = "abcdefghijklmnopqrstuvwxyz"

    for i in range(8):
        ll = 10 + i
        for j in range(8):
            ss = ll - 3 + j
            f = FileUrl(filename=s[0:ss], url="", location="")
            print(f"len: {ll}  {f.getFileName(length=ll)}")
