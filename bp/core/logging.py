import logging
import os
import sys
import tempfile

from bp.configs.app_config import VERBOSE

LOG_FILE = None


def setup_logging():
    """Setup custom logging strategy.

    Anything at INFO level and above (WARN and ERROR) are outputed to the console AND
    the file log.  Anything below (that's DEBUG only), **only** goes to the file log In
    order to capture everything presented to the user, in the file log, if we want to
    print something to the console, it must be done through logging with a
    `log.info("message")`
    """
    global LOG_FILE

    LOG_FILE = os.path.join(tempfile.gettempdir(), "bp.log")
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.DEBUG)

    fileFormatter = logging.Formatter(
        "%(asctime)s [%(threadName)-10.10s] [%(module)-14.14s] "
        "[%(levelname)-5.5s] %(message)s"
    )
    fileHandler = logging.FileHandler(LOG_FILE)
    fileHandler.setFormatter(fileFormatter)
    fileHandler.setLevel(logging.DEBUG)
    rootLogger.addHandler(fileHandler)

    consoleFormatter = logging.Formatter("%(message)s")
    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(consoleFormatter)
    consoleHandler.setLevel(logging.DEBUG if VERBOSE else logging.INFO)
    rootLogger.addHandler(consoleHandler)

    rootLogger.debug("Application startup {}".format("-" * 120))
    rootLogger.debug("Command line params were: {}".format(" ".join(sys.argv[1:])))
