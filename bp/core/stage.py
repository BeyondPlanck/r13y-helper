import sys

import humanize
from bp.configs.bp_config import CONFIG
from bp.impl.downloader import download_requirements


class Stage(object):
    def __init__(self, cfg: dict):
        # print(cfg)
        self.depends = cfg.get("depends")
        self.name = cfg.get("name")
        self.filesets = cfg.get("filesets")

    def print_download_report(self):
        """ Prints an initial download report of required files (for this and
        ALL dependant stages)
        """
        tot_items = 0
        tot_size = 0
        if self.depends:
            for dep in self.depends:
                stage = Stage(CONFIG.get("stages").get(dep))
                items, size = stage.print_download_report()
                tot_items += items
                tot_size += size

        for fsetName in self.filesets:
            fset = CONFIG.get("filesets").get(fsetName)
            if not fset:
                print(f"Ooops! '{fsetName}' not recognized!")
                sys.exit(-1)
            items, size = download_requirements(fset)
            pretty_size = humanize.naturalsize(size, gnu=True)
            print(f"  For '{fset.get('name')}':")
            if items:
                print(f"    {items} files, for a total size of {pretty_size}")
            else:
                print("    Nothing, as everything is already downloaded.")
            tot_items += items
            tot_size += size

        return tot_items, tot_size

    def run(self, report=False):
        """ Start execution of this stage.

        :param report: If true it will make an initial calculation on what files need to
        be downloaded before this stage (or any depended stages) require
        """

        if report:
            print(
                "Before executing this stage we will need to download "
                "the following lists of files:"
            )
            items, size = self.print_download_report()
            pretty_size = humanize.naturalsize(size, gnu=True)
            print(f"For a total number of {items} files and {pretty_size}")
            answer = "z"
            while answer.lower() not in ("y", "n", "yes", "no"):
                answer = input("\nIs it ok to proceed? (y/n) ")

            if answer in ("n", "no"):
                print("Quiting as requested!")
                sys.exit(-1)

        if self.depends:
            for dep in self.depends:
                stage = Stage(CONFIG.get("stages").get(dep))
                stage.run()

        # Download required files
        print(f"Running {self.name}")
