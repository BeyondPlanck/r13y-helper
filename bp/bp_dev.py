import argparse
import logging
import sys
from typing import List, Optional

from bp.commands_dev.cli_parser import parse_args
from bp.commands_dev.ingest import ingest_fileset
from bp.commands_dev.verify import verify_config
from bp.configs.bp_config import CONFIG

logger = logging.getLogger(__name__)

COMMANDS = {
    "ingest": ingest_fileset,
    "verify": verify_config,
}


def dispatch(function, *args, **kwargs):
    """
    Dispatch command line action to proper
    kb function
    """
    return COMMANDS[function](*args, **kwargs)


def main():
    args = parse_args(sys.argv[1:])

    cmd = args.command
    cmd_params = vars(args)

    dispatch(cmd, cmd_params)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        logger.warning("")
        logger.warning("CTRL+C interrupt received, stopping...")
