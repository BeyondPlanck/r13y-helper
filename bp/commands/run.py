import logging
from typing import Dict

from bp.configs.bp_config import CONFIG, FILESETS, STAGE_NAMES
from bp.core.stage import Stage

logger = logging.getLogger(__name__)


def run(args: Dict[str, str]):
    """Execute the requested BeyondPlanck pipeline stage."""

    print(f"Stage: {args['stage']}")

    stage_name = args["stage"]

    if stage_name and stage_name not in STAGE_NAMES:
        logger.error(f"{stage_name} not a valid BeyondPlanck pipeline stage")
        logger.info("Valid stage names are:")
        all_stages = CONFIG.get("stages")
        for s in all_stages:
            logger.info(f"  {s}: {all_stages.get(s).get('name')}")
        return

    stage = Stage(CONFIG.get("stages").get(stage_name))

    stage.run(report=True)
