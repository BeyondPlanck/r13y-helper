import logging
from typing import Dict

import humanize
from bp.configs.bp_config import CONFIG, FILESETS
from bp.impl.downloader import download_all

logger = logging.getLogger(__name__)


def download(args: Dict[str, str]):
    """Download a list of BeyondPlanck filesets.

        :param dryrun: Only check what files will need to be downloaded, but do not
        download any."""

    print(f"Filesets: {args['filesets']}")
    print(f"Dryrun: {args['dryrun']}")
    print(f"Downloaders: {args['downloaders']}")

    # return

    # lowercase all user provided fileset names
    filesets = tuple(elem.lower() for elem in args["filesets"])

    pretty_filesets = []
    totSize = 0
    for fs_name in CONFIG.get("filesets"):

        fs = CONFIG.get("filesets").get(fs_name)
        size = humanize.naturalsize(fs.get("total_size"), gnu=True)
        totSize += fs.get("total_size")
        pretty_filesets.append(f"  {fs_name} (size {size})")

    list.sort(pretty_filesets)

    # add an ALL option to all filesets
    prettyTotSize = humanize.naturalsize(totSize, gnu=True)
    pretty_filesets.append(f"  ALL (size {prettyTotSize})")

    avail_filesets = "\n".join(pretty_filesets)

    if not filesets:
        logger.error("You have to supply at least one fileset to download.")
        logger.info(f"Available filesets are:\n{avail_filesets}")
        return
    elif not all(elem in FILESETS or elem == "all" for elem in filesets):
        logger.error("Invalid list of filesets to download.")
        logger.info(f"Available filesets are:\n{avail_filesets}")
        return

    # If the list of incoming filesets contains 'all', substitute it with
    # all available filesets
    if "all" in filesets:
        filesets = tuple(f for f in FILESETS)

    download_all(filesets, args["dryrun"])
