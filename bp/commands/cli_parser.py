"""
Command Line Parsing Module for bp
"""

__all__ = ()

import argparse
import sys

from bp import __version__


def parse_args(args):
    """
    This function parses the arguments which have been passed from the command
    line, these can be easily retrieved for example by using "sys.argv[1:]".
    It returns a parser object as with argparse.

    Arguments:
    args -- the list of arguments passed from the command line as the sys.argv
            format

    Returns:
    A parser with the provided arguments, which can be used in a
    simpler format
    """
    parser = argparse.ArgumentParser(
        prog="bp", description="BeyondPlanck Reproducibility tool"
    )

    parser.add_argument(
        "--version", action="version", version="%(prog)s {}".format(__version__)
    )

    subparsers = parser.add_subparsers(help="commands", dest="command")
    subparsers.required = True

    # Main Commands
    download_parser = subparsers.add_parser("download", help="Download filesets")
    run_parser = subparsers.add_parser("run", help="Run pipeline stages")

    # download parser
    download_parser.add_argument(
        "filesets",
        help="Comma separated list of filesets to download",
        type=str,
        nargs="*",
    )
    download_parser.add_argument(
        "-n",
        "--downloaders",
        help="Number of concurrent downloaders (overides settings file)",
        type=int,
    )
    download_parser.add_argument(
        "-d",
        "--dryrun",
        help="Perform a dryrun without downloading anything",
        default=False,
        type=bool,
    )

    # run parser arguments

    # making the argument name NOT include a prefix of '-' makes this a
    # positional argument that is a required param
    run_parser.add_argument("stage", help="Pipeline stage to run", type=str)

    if len(args) == 0:
        parser.print_help(sys.stderr)
        sys.exit(1)

    return parser.parse_args(args)
