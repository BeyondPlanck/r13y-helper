# -*- coding: utf-8 -*-

"""Top-level package for Reproducability in Science Command Line Tool."""

__author__ = """Planetek Hellas"""
__email__ = "gerakakis@planetek.gr"
__version__ = "v0.5.15"

from bp.core.logging import setup_logging

setup_logging()
