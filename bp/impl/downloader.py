import logging
import multiprocessing
import sys
import glob

from multiprocessing.pool import ThreadPool
from pathlib import Path
from typing import List

import humanize
from bp.configs.app_config import CONCURRENT_DOWNLOADS, DOWNLOAD_PATH
from bp.configs.bp_config import CONFIG, DOWNLOAD_INPUT_FILES_FOLDER
from bp.core.fileurl import FileUrl
from tqdm import tqdm

mpl = multiprocessing.log_to_stderr()
mpl.setLevel(logging.WARNING)


def final_file_location(location):
    return Path(DOWNLOAD_PATH, DOWNLOAD_INPUT_FILES_FOLDER, location)


def download_fileset(fset: dict, dryrun: bool):
    """Download all files given in a fileset (if not already downloaded)
    """

    if fset:
        if dryrun:
            print(f"Skipping download of fileset: {fset.get('name')}")
            pass
        else:
            location = fset.get("location")
            download_at = final_file_location(location)
            download_at.mkdir(parents=True, exist_ok=True)

            files = fset.get("files")

            pool = ThreadPool(CONCURRENT_DOWNLOADS)

            tot_files = len(files)
            results = []

            for i, aFile in enumerate(files):
                # print(f"working with {aFile[0]}")
                running_count = "{:02d}/{:02d}".format(i + 1, tot_files)
                results.append(
                    pool.apply_async(
                        FileUrl.download_with_progress,
                        args=(aFile, download_at, running_count),
                    )
                )

            pool.close()
            pool.join()

            # Special case when dealing with `auxcmd3` fileset
            # Make substitutions where the L2data have been downloaded
            # on all `filelist_XX.txt` files and create new:
            # `filelist_XX_cm3.txt` files with correct file paths
            l2data_location = final_file_location(
                CONFIG.get("filesets").get("l2data").get("location")
            )
            for aFile in glob.glob("{0}/filelist_*.txt".format(download_at)):
                outFile = aFile.replace(".txt", "_cm3.txt")
                with open(aFile, "rt") as fin:
                    with open(outFile, "wt") as fout:
                        for line in fin:
                            fout.write(
                                line.replace("download_location", str(l2data_location))
                            )

            return results


def check_fileset(fset: dict):
    location = fset.get("location")
    download_at = final_file_location(location)
    download_at.mkdir(parents=True, exist_ok=True)

    files = fset.get("files")

    pool = ThreadPool(4)

    tot_files = len(files)
    results = []

    for i, aFile in enumerate(files):
        # print(f"working with {aFile[0]}")
        running_count = "{:02d}/{:02d}".format(i + 1, tot_files)
        results.append(
            pool.apply_async(
                FileUrl.check_exists, args=(aFile, download_at, running_count),
            )
        )

    pool.close()
    pool.join()

    totSize = 0
    for r in results:
        totSize += r.get()
    return len(results), totSize


def check_fileset_with_progress(fset: dict, workers: int):
    location = fset.get("location")
    download_at = final_file_location(location)
    download_at.mkdir(parents=True, exist_ok=True)

    files = fset.get("files")

    pool = ThreadPool(workers)

    # display a progress bar, but remove it when done!
    pbar = tqdm(total=len(files), leave=False)

    # helper method to update the progress bar
    def update(*a):
        pbar.update()

    tot_files = len(files)
    results = []

    for i, aFile in enumerate(files):
        running_count = "{:02d}/{:02d}".format(i + 1, tot_files)
        results.append(
            pool.apply_async(
                FileUrl.check_exists,
                args=(aFile, download_at, running_count),
                callback=update,
            )
        )

    pool.close()
    pool.join()

    items = 0
    totSize = 0
    for r in results:
        if r.get():
            items += 1
            totSize += r.get()

    return items, totSize


def download_all(filesets: List[str], dryrun: bool):
    """ Iterate over a list of strings, each one a potential fileset name and
    download each.
    Before, starting the process of downloading, display the files that wil be
    downloaded and the required space, and ask user if they still want to proceed,
    giving them an option to bail out.
    """

    tItems, tSize = 0, 0
    print("Download requirements:")
    for fset in sorted(filesets):
        fset_data = CONFIG.get("filesets").get(fset)
        items, size = download_requirements(fset_data)
        pretty_size = f"(size {humanize.naturalsize(size, gnu=True)})" if size else ""
        print(f"  For fileset [{fset:23}]: {items} files {pretty_size}")

        tItems += items
        tSize += size

    if len(filesets) > 1:
        pretty_size = f"(size {humanize.naturalsize(tSize, gnu=True)})" if tSize else ""
        print(f"  {'TOTAL to Download':>37}: {tItems} files {pretty_size}\n")

    if tItems > 0:
        print(f"All files will be downloaded to: {DOWNLOAD_PATH}")
        print("(Note: you can change download location by editing the .bp/config file)")
        reply = input("Proceed with download? [y|n] ")
        if reply.lower() == "y":
            for fset in filesets:
                fset_data = CONFIG.get("filesets").get(fset)
                download_fileset(fset_data, dryrun)
        else:
            print("Aborting download!")
    else:
        print("All required files are already downloaded!")


def download_requirements(fset: dict):
    """ Iterate over a fileset and derive how many files are missing from the local
    download location and what is their total size.

    Return a tuple of (totalFiles, totalSize(in bytes))
    """

    print(f"     Checking [{fset.get('name'):23}] checksums...")

    items, size = check_fileset_with_progress(fset, workers=6)

    # trying to remove the "checking [..." from above
    # cursor up one line
    sys.stdout.write("\x1b[1A")
    # delete last line
    sys.stdout.write("\x1b[2K")

    return items, size


def download_requirements_singleThread(fset: dict):
    """ Iterate over a fileset and derive how many files are missing from the local
    download location and what is their total size.  The single thread version is
    almost 4-5 time slower than a multithreaded version of 6 workers.

    Return a tuple of (totalFiles, totalSize(in bytes))
    """

    totFiles = len(fset.get("files"))
    items = 0
    size = 0
    for i, f in enumerate(fset.get("files")):
        print(
            f"     Checking [{fset.get('name'):23}]: file {i}/{totFiles}...",
            end="\r",
            flush=True,
        )
        aFileUrl = FileUrl(
            f[0], final_file_location(fset.get("location")), f[1], f[2], f[3]
        )
        if not aFileUrl.exists():
            items += 1
            size += aFileUrl.filesize

    print("", end="\r", flush=True)
    # Clear characters to the end of line (weird ASCII sequence)
    sys.stdout.write("\033[K")

    return items, size
