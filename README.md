# BeyondPlanck reproducibility tool

To compile latest version of the tool, you have to create a tag on
master from the Gitlab interface.

`.gitlab-ci.yml` will take over to auto-build linux and windows
releases.

**NOTE**: make sure you always have the `requirements.txt` file updated
until we automate it's autogeneration.  This file has to be updated only
when the `pyproject.toml` file changes.  Then update it with:
`inv dev.deps` and commit it!


## Compiling on Centos

Manually compiling the r13y tool on Centos7 (in order to get access to
an old version of Glibc, 2.17):

```
docker run -u root --rm -it -v $(pwd)/requirements.txt:/app/reqs.txt  -v $(pwd)/bp.spec:/app/bp.spec -v $(pwd)/bp:/app/bp centos/python-36-centos7 bash
```

From the prompt:

```
cd /app
pip install -r reqs.txt
pyinstaller bp.spec -y --onefile
cp dist/bp bp/bp-centos
exit
```

The compiled binary `bp-centos` should now be in `bp/bp-centos`
