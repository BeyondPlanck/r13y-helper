#!/bin/sh

./build_cm3_image.sh

# echo $1
docker tag bp/cm3 registry.gitlab.com/beyondplanck/r13y-helper/cm3:$1
docker tag bp/cm3 registry.gitlab.com/beyondplanck/r13y-helper/cm3:latest
docker push registry.gitlab.com/beyondplanck/r13y-helper/cm3:$1
docker push registry.gitlab.com/beyondplanck/r13y-helper/cm3:lastest