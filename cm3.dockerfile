# Docker file for stage 4:
# https://gitlab.com/BeyondPlanck/repo/issues/129

# based on our base image
FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && \
    apt-get install -y \
    apt-utils \
    autoconf \
    automake \
    bison \
    build-essential \
    ca-certificates \
    cmake \
    cmake-data \
    curl \
    g++ \
    gfortran \
    git \
    gsl-bin \
    libgsl-dev \
    libhdf5-dev \
    libopenblas-dev \
    libssl-dev \
    libtool \
    mpi-default-dev \
    nano \
    openmpi-bin \
    openssh-server \
    pkg-config \
    python \
    python3 \
    texinfo \
    wget \
    zlib1g \
    zlib1g-dev && \
    #----------------------------------
    # cleanup
    rm -rf /var/lib/apt/lists/*

#######################################################################
# Upgrade cmake to 3.17
RUN mkdir -p /code/cmake && \
    cd /code/cmake && \
    wget https://github.com/Kitware/CMake/releases/download/v3.17.3/cmake-3.17.3.tar.gz && \
    tar -xzvf cmake-3.17.3.tar.gz && \
    cd cmake-3.17.3 && \
    ./bootstrap && \
    make -j 8 && \
    make install && \
    rm -rf /code/cmake


#######################################################################
# Install commander3
RUN mkdir /cm3_install && \
    cd /cm3_install &&\
    git clone https://github.com/Cosmoglobe/Commander.git &&\
    cd Commander && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=Release \
          -DCMAKE_C_COMPILER=gcc \
          -DCMAKE_CXX_COMPILER=g++ \
          -DCMAKE_Fortran_COMPILER=gfortran \
          -DMPI_C_COMPILER=mpicc \
          -DMPI_CXX_COMPILER=mpic++ \
          -DMPI_Fortran_COMPILER=mpifort .. && \
          cmake --build . --target install -j 8 && \
    rm -rf /cm3_install

ENV HEALPIX=/usr/local/healpix
