================================================================================
Files downloaded by the r13y tool are downloaded from the Cosmoglobe
server.

This is a three Phase process:

* Phase 1: Collect all the files to be distributed in a common folder in owl.
* Phase 2: rsync the owl folder with a publicly available Cosmoglobe folder.
* Phase 3/: Run some pre-processing to generate some config files



## Phase 1: Collect all files

Steps:

1. We move files to an owl accessible folder at (usually):
   * /mn/stornext/u3/trygvels/compsep/cdata/like/BP_releases/input

2. We have to group the files according to the supported r13y filesets (see a full list
  in Phase 3)

    The final folders should look like:

      * /mn/stornext/u3/trygvels/compsep/cdata/like/BP_releases/input/filesetA
      * /mn/stornext/u3/trygvels/compsep/cdata/like/BP_releases/input/filesetB
      * /mn/stornext/u3/trygvels/compsep/cdata/like/BP_releases/input/filesetXXX


3. On the each owl folder that hosts a fileset:
     * Run the ./etc/gen_ingestion_file.sh to generate md5 hashes on the source file so
    the r13y tool does NOT  have to download the file locally in order to get the
    hashes.  Run it as:
    *  ~/gen_ingestion_file.sh . > some_fileset.txt
    *  Download the generated `some_fileset.txt` to our local development folder
    *  and remove it from the fileset folder (it is not required there anymore)
    *  Utilize the `some_fileset.txt` as described in Phase 3


## Phase 2

Steps:

1. We then notify an IT administrator to rsync the files from owl to cosmoglobe
   * (Usually) Stein Vidar Hagfors Haugan <s.v.h.haugan@astro.uio.no>
   * Stein Vidar knows his voodoo but the idea is something like:
   * rsync -arv /owl/some/path/BP_releases/input /cosmoglobe/root-folder/BP_releases
2. This would make all files publicly available under a url. We NEED to know the root
  folder of where the files are published on Cosmoglobe. For example, if they are
  published as:
   * http://cosmoglobe.uio.no/BeyondPlanck/releases/input/beams/some_beam.file
   * we care about the (usually):
     * http://cosmoglobe.uio.no/BeyondPlanck/releases/input
   * part!


## Phase 3: Prepare them for ingestion

Currently the r13y tool supports the download of the following filesets:

```
  adc_corrections (size 1.9M)
  attitude_history_files (size 158.8G)
  auxcmd3 (size 38.3G)
  beamalms (size 179.7M)
  beams (size 281.6M)
  checkpoints (size 185.6K)
  diode_weights (size 8.4K)
  dipole_convolve_params (size 11.2K)
  haslam_maps (size 12.0M)
  l1data (size 8.4T)
  l2data (size 880.1G)
  lfi_rimo (size 753.8K)
  mask_calib (size 9.0M)
  npipe_covmaps (size 6.1G)
  npipe_hitmaps (size 55.7G)
  npipe_noise (size 4.1K)
  npipe_skymaps (size 8.3G)
  ringsets (size 20.6G)
  sampling_lists (size 9.5M)
  satellite_velocity (size 69.9M)
  sidelobealms (size 35.6M)
  sidelobes (size 87.4M)
  spikes_templates (size 101.2K)
  subsampled_housekeeping (size 9.7M)
  wmap_maps (size 480.1M)
  ALL (size 9.5T)
```

*(Sizes are current as of their latest update, aprox Oct 2020)*

New filesets CAN be added, and existing filesets CAN be removed.  They are split in
multiple filesets to allow users to download ONLY what they need.


The steps to update (or add) a given fileset to its latest version are:

1. Use the `some_fileset.txt` created from Phase 1, into our local dev environment (we
  usually store them at `./cfg/filesets/some_fileset.txt`)

1. The MD5 generation script unfortunately includes itself in the list of produced files.
  Edit the `some_fileset.txt` file to remove the reference to a `./some_fileset.txt`
  file.

1. The file contains relative filepaths that have to be changed to absolute urls, by
  doing a search and replace in that file and use the base url of:
  `http://cosmoglobe.uio.no/BeyondPlanck/releases/input` (again. check Phase 2 on what
  is the proper_url. It is always the same, but double check)
   * That way the ingestion process can verify that the actual final urls indeed resolve
    to valid download urls

1. Update the `./bp/configs/bp_config.json` file:

   * If we are creating a new fileset, add the new fileset definition inside the:

    ```
    `filesets` dict:

      "fileset_name": {
            "name": "Pretty Fileset Name",
            "location": "path_where_to_store",
            "files": []
      }
      ```

      example:

    ```
      "adc_corrections": {
            "name": "ADC Corrections",
            "location": "adc_corrections",
            "files": [],
            "total_size": 0
      }
    ```

    `files` is an empty array (it will be populated with the ingest command)

    * If we are updating an existing fileset, no need to do anything.  The ingest
      command will override the `files` attribute.

1. Run the ingest command:
    * `python -m bp.bp_dev ingest -f fileset_name -u cfg/filesets/some_fileset.txt`
    * The command will iterate over **all** files making sure URLS are correctly
      resolving and it will populate `/bp/configs/bp_config.json` with correct values.

1. Verify that the new config file is correct:
   * `python -m bp.bp_dev verify`

1. Run a test command to make sure the new fileset appears in the download options:
   * `python -m bp.bp download`
   * You should see the new fileset appear in the list of available filesets.

