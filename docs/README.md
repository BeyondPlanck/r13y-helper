# Running tool

To run from within the dev env:

python -m bp.bp


# Generate final release bin

pyinstaller -y --onefile bp.spec

Final executable will be in /dist

Note that running the above command on linux generates a linux app.

We will need to run the same command on a Windows/Mac to generate other
apps.

**NOTE**: Generating the final executable on a random developers machine
might end up with a binary that does NOT work on all envs.  See
`TROUBLESHOOTING.md` for more info

Runnning it through our Gitlab CI pipeline seems to **always** work.



# Notes on other tools generating single file executables

* cx_freeze: does not compile everything into one executable file only!

