The compiled bp binary, generated with pyinstaller might fail with:


$ ./bp
[452025] Error loading Python lib
'/tmp/_MEIxHqIbi/libpython3.7m.so.1.0': dlopen: /lib64/libc.so.6:
version `GLIBC_2.25' not found (required by
/tmp/_MEIxHqIbi/libpython3.7m.so.1.0)


This is documented issue with pyinstaller:
* https://github.com/DARIAH-DE/TopicsExplorer/issues/53#issuecomment-397311388

Bottom line: GLIBC versions matter!  Binaries compiled with later
versions of GLIBC does not run on machines with older versions. 

Check GLIBC version with: `ldd --version`

Proposed Solution: generate the binary in an as older machine as
possible! Ouch!