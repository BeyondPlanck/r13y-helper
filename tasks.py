# -*- coding: utf-8 -*-
"""Collection of invoke tasks"""
from __future__ import absolute_import, unicode_literals

import os
import sys
import webbrowser
from datetime import datetime
from urllib.request import pathname2url

from invoke import Collection, task

ns = Collection()

dev = Collection("dev")
dist = Collection("dist")

ns.add_collection(dev)
ns.add_collection(dist)

ROOT = os.path.dirname(__file__)

CLEAN_PATTERNS = [
    "build",
    "dist",
    "cover",
    "docs/_build",
    "**/*.pyc",
    ".tox",
    "**/__pycache__",
    "reports",
    "*.egg-info",
]


def color(code):
    """A simple ANSI color wrapper factory"""
    return lambda t: "\033[{0}{1}\033[0;m".format(code, t)


GREEN = color("1;32m")
YELLOW = color("1;33m")
RED = color("1;31m")
BLUE = color("1;30m")
CYAN = color("1;36m")
PURPLE = color("1;35m")
WHITE = color("1;39m")


def header(text):
    """Display a header"""
    print(" ".join((BLUE(">>"), CYAN(text))))
    sys.stdout.flush()


def info(text, *args, **kwargs):
    """Display information"""
    text = text.format(*args, **kwargs)
    print(" ".join((PURPLE(">>>"), text)))
    sys.stdout.flush()


def success(text):
    """Display a success message"""
    print(" ".join((GREEN(">>"), WHITE(text))))
    sys.stdout.flush()


def error(text):
    """Display an error message"""
    print(RED("✘ {0}".format(text)))
    sys.stdout.flush()


def warn(text):
    """Display a warning message"""
    print(YELLOW("o {0}".format(text)))
    sys.stdout.flush()


def custom_exit(text=None, code=-1):
    """Exit with a specific error message and error code"""
    if text:
        error(text)
    sys.exit(code)


def build_args(*args):
    """Build a list of arguments"""
    return " ".join(a for a in args if a)


@task
def dev_clean(ctx):
    """Cleanup all build artifacts"""
    header(dev_clean.__doc__)
    with ctx.cd(ROOT):
        for pattern in CLEAN_PATTERNS:
            info("Removing {0}", pattern)
            ctx.run("sudo rm -rf {0}".format(pattern))


dev.add_task(dev_clean, name="clean")


@task()
def dev_deps(ctx):
    """Install or update development dependencies"""
    header(dev_deps.__doc__)
    # update current venv
    header("Updating existing dependencies")
    ctx.run("poetry update", pty=True, echo=True)
    header("Generating pip requirements.txt...")
    # TODO: This generates requirements for `prod` AND `dev` env
    # we need to have a way to generate only the `prod` requirements
    # (all packages in [tool.poetry.dependencies]  of `pyproject.toml` only
    # and NOT [tool.poetry.dev-dependencies])
    ctx.run(
        "poetry export -f requirements.txt -o requirements.txt " "--without-hashes",
        pty=True,
        echo=True,
    )
    warn(
        "Make sure that all new dependencies work fine before commiting.  "
        "Run a local test!"
    )


dev.add_task(dev_deps, name="deps")

# ======================================================================================

@task()
def dev_deps(ctx):
    """Install all python dependencies from the poetry.lock file."""
    header(dev_deps.__doc__)
    with ctx.cd(ROOT):
        info(f"Running in Root folder: {ROOT}")
        ctx.run("rm -rf .venv", pty=True)
        ctx.run("poetry config virtualenvs.in-project true", pty=True)
        ctx.run("poetry config virtualenvs.path '.'", pty=True)
        with open(".python-version", "r") as file:
            python_version = file.read().replace("\n", "")
        info(f"Using python version: {python_version}")
        ctx.run(f"poetry env use {python_version}", pty=True)
        ctx.run("poetry run pip install --upgrade pip", pty=True)
        # ctx.run(
        #     "poetry run pip install --upgrade --no-cache-dir setuptools==57.5.0",
        #     pty=True,
        # )
        ctx.run("poetry run pip install wheel", pty=True)
        ctx.run("poetry install", pty=True)


dev.add_task(dev_deps, name="deps_install")

@task
def dev_format(ctx):
    """Format source files and sort python imports"""
    header(dev_format.__doc__)
    with ctx.cd(ROOT):
        ctx.run("isort -rc bp")
        ctx.run("black bp")


dev.add_task(dev_format, name="format")


@task
def dist_build(ctx):
    """Build the executable"""
    header(dist_build.__doc__)
    with ctx.cd(ROOT):
        ctx.run("pyinstaller -y --onefile bp.spec")


dist.add_task(dist_build, name="build")


@task(dev_clean, dev_deps, default=True)
def dev_all(ctx):  # pylint: disable=unused-argument, redefined-builtin
    """Clean and test"""


dev.add_task(dev_all, name="all")
