
#!/bin/bash
# Change short urls to the real LONG ones

sed -i -e 's/cosmo_url/http\:\/\/cosmoglobe.uio.no\/BeyondPlanck\/releases\/input/g' $1 
sed -i -e 's/pla_url/http\:\/\/pla.esac.esa.int\/pla\/aio/g' $1