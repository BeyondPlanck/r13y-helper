#!/bin/bash

for file in $(find ~+ -type f | sort);
  do
    md5=$(md5sum "$file")
    size=$(stat -c%s "$file")
    printf "%s %s\n" "$md5" "$size";
  done