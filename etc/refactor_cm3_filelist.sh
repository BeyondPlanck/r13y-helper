#!/bin/bash

# Set the new file prefix here
PREFIX="v17"


# Copy filelist_30_XX.txt to cosmoglobe
cp /mn/stornext/d8/ITA/hke/commander3/v2/data_BP8/filelist_30_${PREFIX}.txt \
   /mn/stornext/u3/trygvels/compsep/cdata/like/BP_releases/input/commander3_aux/filelist_30.txt

# Copy filelist_44_XX.txt to cosmoglobe
cp /mn/stornext/d8/ITA/hke/commander3/v2/data_BP8/filelist_44_${PREFIX}.txt \
   /mn/stornext/u3/trygvels/compsep/cdata/like/BP_releases/input/commander3_aux/filelist_44.txt

# Copy filelist_70_XX.txt to cosmoglobe
cp /mn/stornext/d8/ITA/hke/commander3/v2/data_BP8/filelist_70_${PREFIX}.txt \
   /mn/stornext/u3/trygvels/compsep/cdata/like/BP_releases/input/commander3_aux/filelist_70.txt

# substitute hardcoded file path references to a well known token
sed -i 's/\/mn\/stornext\/d16\/cmbco\/bp\/mathew\/test/download_location/' \
   /mn/stornext/u3/trygvels/compsep/cdata/like/BP_releases/input/commander3_aux/filelist_*.txt

# generate ms5ums and filesize info
for file in $(find . | grep 'filelist_..\.txt' | sort);
  do
    md5=$(md5sum "$file")
    size=$(stat -c%s "$file")
    printf "%s %s\n" "$md5" "$size";
  done