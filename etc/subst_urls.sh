
#!/bin/bash
# Change really LONG urls to shorter ones

sed -i -e 's/http\:\/\/cosmoglobe.uio.no\/BeyondPlanck\/releases\/input/cosmo_url/g' $1 
sed -i -e 's/http\:\/\/pla.esac.esa.int\/pla\/aio/pla_url/g' $1